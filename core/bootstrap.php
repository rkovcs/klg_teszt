<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.04.05.
 * Time: 19:11
 */
date_default_timezone_set('UTC');
setlocale(LC_ALL, 'en_US.utf-8');

class Bootstrap{
	/**
	 * Loading components
	 */
	public function __construct(){
		spl_autoload_register(function ($file) {
			$file = lcfirst($file);
			(is_file( COREPATH.$file.EXT ))?include_once COREPATH.$file.EXT:false;
			(is_file( CONTPATH.$file.EXT ))?include_once CONTPATH.$file.EXT:false;
			(is_file( MODELPATH.$file.EXT ))?include_once MODELPATH.$file.EXT:false;
			(is_file( COREPATH.'exception'.DIRECTORY_SEPARATOR.$file.EXT ))?include_once COREPATH.'exception'.DIRECTORY_SEPARATOR.$file.EXT:false;
		});
	}

	/**
	 * Start default controller
	 * @param null $controller
	 */
	public function start($startController)
	{
		// Mysql connect DB
		new Connect();
		$controller = new Controller();
		$controller->run( $startController );
	}
}