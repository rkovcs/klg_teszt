<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */
class Controller extends Core{


	public $view;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $controller
	 * @throws HttpException
	 */
	public function run($controller)
	{
		$actionPrefix = 'action_';
		if($controller || Core::app()->request->getRequest( 'c' ))
		{
			$exp = explode( '|',Core::app()->request->getRequest( 'c' ));
			$controller = isset($exp[0]) && $exp[0]?$exp[0]:$controller;
			$action = isset($exp[1])?$actionPrefix.$exp[1]:$actionPrefix.'index';
			$param = isset($exp[2])?$exp[2]:false;
		}
		else
		{
			$action = false;
			$param	= false;
		}
		if($controller && $action)
		{
			if(is_file( CONTPATH.'controller'.ucfirst(strtolower($controller)).EXT ))
			{
				$class = 'Controller'.ucfirst( strtolower( $controller ) );
				$controllerObj = new $class();
				$controllerObj->$action($param);
			}
			else
			{
				throw new HttpException( 404,'Page not found.' );
			}
		}else{
			throw new HttpException( 404,'Page not found.' );
		}
	}
}