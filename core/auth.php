<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class Auth extends Core{

	public function __construct(){}

	public function logged(){
		return (isset($_SESSION['user_logged']) && $_SESSION['user_logged'])?true:false;
	}

	public function login($userName = null,$password = null){
		if($userName && $password){
			$userModel = new Model('user');
			$user = $userModel->model->find(array('username'=>$userName));
			if($user){
				if (crypt($userName.$password, $user['password']) == $user['password']) {
					$_SESSION['user_logged'] = true;
					$_SESSION['user_profile'] = array('name'=>$userName,'last_login'=>date("Y-m-d H:i:s"));
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
	}

	public function logout(){
		session_destroy();
	}

}