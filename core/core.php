<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.04.05.
 * Time: 19:30
 */
class Core{
	/**
	 * @var $_REQUEST
	 */
	public $request;
	/**
	 * @var $this
	 */
	public static $app;
	/**
	 * @var Auth
	 */
	public $auth;
	/**
	 * @var User_Profile
	 */
	public $user;

	public function __construct()
	{
		self::$app = $this;
		$this->request	= new Request();
		$this->auth		= new Auth();
		$this->user		= new UserProfile();
	}

	/**
	 * @return Core
	 */
	public static function app()
	{
		return self::$app;
	}

	public function redirect($pathArray = array()){
		$path = '.';
		if(!empty($pathArray))
		{
			$path = '?c='.$pathArray[0];
			if(isset($pathArray[1]))$path.= '|'.$pathArray[1];
			if(isset($pathArray[2]))$path.= '|'.$pathArray[2];
		}
		header('Location: '.$path);
	}

}