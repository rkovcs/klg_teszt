<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.04.05.
 * Time: 21:41
 */

class Validate extends Core{
	/**
	 * @var array
	 */
	private $errors = array();

	public function __construct(){}

	/**
	 * @var array
	 */
	public $rules = array();

	/**
	 * @param array $error
	 */
	public function setErrors($error = array())
	{
		$this->errors = array_merge($this->errors,$error);

	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return string
	 */
	public function getErrorList()
	{
		$errorList = '<ul>';
		foreach($this->getErrors() AS $field => $error)
		{
			if($error)
			{
				$errorList.='<li>'.$error.'</li>';
			}
		}
		$errorList.= '</ul>';
		return $errorList;
	}

	/**
	 * @param array $rules
	 */
	public function setRules($rules = array())
	{
		$this->rules = $rules;
	}

	public function getRules()
	{
		return $this->rules;
	}

	public function isValidate()
	{
		foreach($this->getRules() AS $field => $rule){
			if(isset($rule['regexp']))
			{
				if(!preg_match("/".$rule['regexp']."/",Core::app()->request->getRequest($field)))
				{
					$this->setErrors(array($field=>$rule['msg']));
				}
			}if(isset($rule['email']))
			{
				if(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/",Core::app()->request->getRequest($field)))
				{
					$this->setErrors(array($field=>'Nem megfelelő az e-mail cím formátuma!'));
				}
			}
			if(isset($rule['numeric']))
			{
				if(!is_numeric(Core::app()->request->getRequest($field)))
				{
					$this->setErrors(array($field=>$rule['label']. 'nem szám!'));
				}
			}
			if(isset($rule['callback']))
			{
				$this->$rule['callback'](array('values'=>Core::app()->request->getRequest($field),'field'=>$field,'error_msg'=>$rule['msg']));
			}

			if(isset($rule['required']))
			{
				if(Core::app()->request->getRequest($field) == ''){
					$this->setErrors(array($field=>$rule['label'].' kitöltése kötelező!'));
				}
			}

		}
		if(count($this->getErrors()) > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * @param $phones
	 */
	public function morePhone($param = array())
	{
		$exp = explode(',',$param['values']);
		foreach($exp AS $phone){
			if(!is_numeric($phone)){
				$this->setErrors(array($param['field']=>$param['error_msg']));
			}
		}
	}

}
