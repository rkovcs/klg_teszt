<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class Model extends Core{
	/**
	 * @var Object
	 */
	public $model;
	/**
	 * @var
	 */
	private $pkValue = null;

	public function __construct($model,$param = array())
	{
		if(is_file(MODELPATH.$model.EXT))
		{
			$class = ucfirst($model);
			$this->model = new $class($param);
		}else{
			return false;
		}
	}

	/**
	 * @param array $condition
	 * @return array|bool
	 */
	public function find( $condition = array())
	{
		if($this->table)
		{
			$sql = 'SELECT * FROM '.$this->table.' WHERE ';
				if(!empty($condition))
				{
					foreach($condition AS $field => $value)
					{
						$value = stripslashes(mysql_real_escape_string($value));
						$value = is_string($value)?'"'.$value.'"':$value;
						$sql.= 	$field.'='.$value.' AND';
					}
					$sql = substr($sql,0,-3);
				}
			$r = mysql_query($sql);
			if($r)
			{
				if(mysql_num_rows($r) > 0)
				{
					$return = mysql_fetch_assoc($r);
					$this->pkValue = $return[$this->pk];
					return $return;
				}else{
					false;
				}
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * @return array
	 */
	public function findAll()
	{
		if($this->table)
		{
			$r = mysql_query('SELECT * FROM '.$this->table);
			if($r)
			{
				if(mysql_num_rows($r) >0){
					return $r;
				}else{
					false;
				}
			}
		}
	}

	/**
	 * @param array $values
	 */
	public function save($values = array())
	{
		$st = ($this->pkValue)?'UPDATE ':'INSERT INTO ';
		$sql = $st.$this->table.' SET ';
		if(!empty($values))
		{
			foreach($values AS $field => $value)
			{
				$value = stripslashes(mysql_real_escape_string($value));
				$value = is_string($value)?'"'.$value.'"':$value;
				$sql.= 	$field.'='.$value.',';
			}
			$sql = substr($sql,0,-1);
			if($this->pkValue)
			{
				$sql.= 'WHERE '.$this->pk.'='.$this->pkValue;
			}
			mysql_query($sql);
			return ($this->pkValue)?$this->pkValue:mysql_insert_id();
		}
		return false;
	}

	/**
	 * @param array $condition
	 */
	public function delete($condition = array())
	{
		$sql = 'DELETE FROM '.$this->table;
		if(!empty($condition))
		{
			$sql.= ' WHERE ';
			foreach($condition AS $field => $value)
			{
				$value = stripslashes(mysql_real_escape_string($value));
				$value = is_string($value)?'"'.$value.'"':$value;
				$sql.=$field.'='.$value.' AND';
			}
			$sql = substr($sql,0,-3);

		}
		mysql_query($sql);
	}
}