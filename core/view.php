<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.04.05.
 * Time: 20:05
 */
class View extends Core {

	public $template = '';
	public $dir = '';

	public function __construct(){
		$this->dir = APPPATH.'views'.DIRECTORY_SEPARATOR;
	}
	/**
	 * @param string $file
	 * @return bool
	 */
	function View($file='')
	{
		return $this->set_file($file);
	}

	/**
	 * @param $values
	 * @param string $file
	 * @return bool|string
	 */
	private function load($values, $file='')
	{
		$hasTemplate = !empty($this->template);

		if ( !empty($file) )
		{
			$hasTemplate = $this->set_file($file);
		}

		if ( !$hasTemplate )
		{
			return false;
		}

		foreach($values AS $index => $value){
			${$index} = $value;
		}
		ob_start();
		include $this->dir . $this->template;
		$contents = ob_get_contents();
		ob_end_clean();

		return $contents;
	}

	/**
	 * @param $values
	 * @param string $file
	 */
	public function display($values, $file='')
	{
		$contents = $this->load($values, $file);
		if ( $contents ) {
			echo $contents;
		}
	}

	/**
	 * @param array $params
	 */
	public function renderView($path = false,$params = array() )
	{
		if($path){
			$this->dir = DOCROOT.str_replace('_',DIRECTORY_SEPARATOR,$path).DIRECTORY_SEPARATOR;
		}
		$this->set_file($params[0]);
		$this->display($params[1]);
	}

	/**
	 * @param $file
	 * @return bool
	 */
	public function set_file($file)
	{
		if ( !empty($file) && file_exists($this->dir.$file.EXT) )
		{
			$this->template = $file.EXT;
		}
      else {
			return false;
		}
   }
}