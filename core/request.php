<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class Request extends Core{

	/**
	 * @var $_REQUEST
	 */
	public static $req;

	/**
	 *
	 */
	public function __construct(){
		self::$req = $_REQUEST;
	}

	/**
	 * @param $param $_REQUEST[key]
	 */
	public function getRequest($key = null)
	{
		if($key)
		{
			return isset( self::$req[$key] )?self::$req[$key]:false;
		}
		else
		{
			return self::$req;
		}
	}

	/**
	 * @return bool
	 */
	public function isPost(){
		return !empty($_POST)?true:false;
	}
}