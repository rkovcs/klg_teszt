<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class UserProfile extends Core{
	/**
	 * @var $_SESSION['user_profile']
	 */
	private $profile;

	public function __construct()
	{
		if(isset($_SESSION['user_profile']))
		{
			$this->profile = $_SESSION['user_profile'];
		}
	}

	public function getProfile($key = null)
	{
		if($key && isset($this->profile[$key]))
		{
			return $this->profile[$key];
		}
	}
}