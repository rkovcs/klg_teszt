<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 * Date: 2013.04.05.
 * Time: 29:52
 */
class HttpException extends Exception{
	public function __construct($code,$msg){
		echo '<b>HttpException: </b>'.$code;
		echo '<br><b>Messages: </b>'.$msg;
		echo '<hr><b>Call stack: </b><br>';
		echo '<pre>',print_r( debug_backtrace() ),'</pre>';
	}
}