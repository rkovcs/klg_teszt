<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */
class Connect extends Core{
	/**
	 * @var MySQL link
	 */
	public static $connect = false;
	/**
	 * @var string
	 */
	private $username = 'root';
	/**
	 * @var string
	 */
	private $password = '';
	/**
	 * @var string
	 */
	private $host = 'localhost';
	/**
	 * @var string
	 */
	private $dbName = 'klg_teszt';


	public function __construct(){
		if(!self::$connect){
			self::$connect = mysql_connect($this->host, $this->username, $this->password)or die("Cannot connect mysql!");
			mysql_select_db($this->dbName)or die("Cannot select DB");
		}
	}

}