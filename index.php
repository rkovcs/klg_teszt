<?php
/**
* Created by JetBrains PhpStorm.
 * Date: 2013.04.05.
 * Time: 17:47
 * User: Kovács Róbert
 */
session_start();
header('Content-Type: text/html; charset=utf-8');
define('EXT','.php');
define('DEFAULT_CONTROLLER','index');
/**
 * Define the absolute paths
 */
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR); 							// Document root
define('APPPATH', realpath('application').DIRECTORY_SEPARATOR);									// Application
define('CONTPATH', APPPATH.'controllers'.DIRECTORY_SEPARATOR);									// Controllers
define('MODELPATH', APPPATH.'models'.DIRECTORY_SEPARATOR);										// Models
define('COREPATH', realpath('core').DIRECTORY_SEPARATOR);										// System core
define('ASSETSPATH', 'application'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR);			// Assets

$bootstrap = COREPATH.'bootstrap'.EXT;
if(is_file($bootstrap))
{
	include_once $bootstrap;
	try {
		/**
		 * @var Bootstrap
		 */
		$bootObj =  new Bootstrap();
		$bootObj->start(DEFAULT_CONTROLLER);

	} catch (Exception $e) {
		die('Error bootstrap start.');
	}
}

