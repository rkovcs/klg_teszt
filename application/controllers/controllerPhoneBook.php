<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */
class ControllerPhoneBook extends Controller{

	public function __construct()
	{
		$this->view = new View();
	}

	/**
	 *
	 */
	public function action_index()
	{
		if(!Core::app()->auth->logged())Core::app()->redirect();

		$this->view->set_file('phoneBook');
		$phoneBookModel = new Model('phoneBook');
		$phoneBooks = $phoneBookModel->model->findAllPhoneBookItem();

		$elements = '';
		while($row = mysql_fetch_assoc($phoneBooks)){
			$elements .= '<tr>';
			$elements .= '<td>'.$row['first_name'].'</td>';
			$elements .= '<td>'.$row['last_name'].'</td>';
			$elements .= '<td>'.$row['email'].'</td>';
			$elements .= '<td>'.$row['phone'].'</td>';
			$elements .= '<td><a href="javascript:if(confirm(\'Biztos törli? Figyelem! Az összes telefonszám törlődni fog! Ha csak egy számot szeretne törlni, azt a szerkesztés oldalon teheti meg.\'))window.location =\'?c=phoneBook|delete|'.$row["id"].'\'">Delete</a>&nbsp<a href="?c=phoneBook|edit|'.$row["id"].'">Edit</a></td>';
			$elements .= '</tr>';
		}


		$this->view->display(array('elements'=>$elements));
	}

	/**
	 * @param null $phoneBookId
	 */
	public function action_edit($phoneBookId = null)
	{
		if(!Core::app()->auth->logged())Core::app()->redirect();

		$this->view->set_file('edit');
		$validate = new Validate();
		$viewVariables = array('validate'=>$validate);
		$phoneBookModel = new Model('phoneBook');
		$phoneModel = new Model('phone');
		if(Core::app()->request->isPost())
		{
			$validate->setRules(array(
				'first_name'=>array(
									'label'=>'First name'
									,'required'=>1
									,'regexp'=>'^[a-zA-Z]+$'
									,'msg'=>'Nem megfelelő a a First name formátuma!'
				)
				,'last_name'=>array(
									'label'=>'Last name'
									,'required'=>1
									,'regexp'=>'^[a-zA-Z]+$'
									,'msg'=>'Nem megfelelő a a Last name formátuma!'
				)
				,'email'=>array(
								'label'=>'E-mail'
								,'required'=>1
								,'email'=>1
				)
				,'phone'=>array(
								'label'=>'Phone'
								,'required'=>1
								,'callback'=>'morePhone'
								,'msg'=>'A telefonszámok csak szám formátumúak lehetnek, vesszővel elválasztva!'
				)
			));
			if($validate->isValidate())
			{
				if(Core::app()->request->getRequest('id') != '')
				{
					$phoneBookModel->model->find(array('id'=>Core::app()->request->getRequest('id')));
					$phoneModel->model->delete(array('id_phonebook'=>Core::app()->request->getRequest('id')));
				}
				// Save (insert or update) phone book
				$insertId = $phoneBookModel->model->save(array(
													'first_name'=>Core::app()->request->getRequest('first_name')
													,'last_name'=>Core::app()->request->getRequest('last_name')
													,'email'=>Core::app()->request->getRequest('email')
													));
				// Save phone number
				$expPhone = explode(',',Core::app()->request->getRequest('phone'));

				foreach($expPhone AS $phone)
				{
					$phoneModel->model->save(array('phone'=>$phone,'id_phonebook'=>$insertId));
				}
				// Redirect
				Core::app()->redirect(array('phoneBook','index'));


			}else{
				foreach(Core::app()->request->getRequest() AS $field => $value)
				{
					$viewVariables[$field] = $value;
				}
			}
		}
		elseif($phoneBookId)
		{
			$phoneBook = $phoneBookModel->model->find(array('id'=>$phoneBookId));
			if($phoneBook){
				$viewVariables = array_merge($viewVariables,$phoneBook);
				$phoneNum = $phoneModel->model->getPhoneBookItemNumbers($phoneBookId);
				$numbers = '';
				while($row = mysql_fetch_assoc($phoneNum)){
					$numbers.=$row['phone'].',';
				}
				$numbers = substr($numbers,0,-1);
				$viewVariables['phone'] = $numbers;
			}else{
				Core::app()->redirect(array('phoneBook','edit'));
			}
		}
		$this->view->display($viewVariables);
	}

	/**
	 * @param $phoneBookId
	 */
	public function action_delete($phoneBookId)
	{
		if(!Core::app()->auth->logged())Core::app()->redirect();

		$phoneBookModel = new Model('phoneBook');
		// Delete phone book (trigger(phoneBookDelete) - delete phone numbers)
		$phoneBookModel->model->delete(array('id'=>$phoneBookId));
		Core::app()->redirect(array('phoneBook','index'));
	}
}