<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class ControllerIndex extends Controller{

	public function __construct(){
		$this->view = new View();
	}

	public function action_index(){
		if(Core::app()->auth->logged()){
			Core::app()->redirect(array('phoneBook'));
		}else{
			$this->action_login();
		}
	}

	public function action_login(){
		$this->view->set_file('login');
		$validate = new Validate();
		if(Core::app()->request->isPost()){
			if(Core::app()->auth->login(
									Core::app()->request->getRequest('user_name'),
									Core::app()->request->getRequest('password')
									))
			{
				Core::app()->redirect();
		}
		else
		{
			$validate->setErrors(array('user_name'=>'Hibás felhasználónév vagy jelszó','password'=>null));
		}
		}
		$this->view->display(array('validate'=>$validate));
	}

	public function action_logout(){
		Core::app()->app()->auth->logout();
		Core::app()->redirect();
	}
}