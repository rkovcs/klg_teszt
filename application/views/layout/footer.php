</div>
<?php
if(isset($validate))
{
?>
<script type="text/javascript">
$(document).ready(function() {
	<?php
	$errors = $validate->getErrors();
	if(!empty($errors))
	{
		foreach($errors AS $field => $error){
			echo "$('#".$field."').css('border-color','red');";
		}
		echo "$('#error_box').css('display','block');";
		echo "$('#error_box').html('".$validate->getErrorList()."');";
	}
	?>
});
</script>
<?php
}
?>
<?php $this->renderView('core_layout',array('footer',array())) ?>