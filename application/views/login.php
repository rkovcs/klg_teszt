<?php $this->renderView('application_views_layout',array('header',array())) ?>
<div class="login">
	<div class="login_box">
		<fieldset><legend>KLG-teszt login</legend>
        <div id="error_box"></div>
		<form action="" method="POST" id="login_form">
			<div class="item"><label>Felhasználónév:</label><input name="user_name" id="user_name" type="text"></div>
			<div class="item"><label>Jelszó:</label><input name="password" id="password" type="password"></div>
			<div class="item"><input type="submit" value="Belépés"> </div>
        </form>
        </fieldset>
	</div>
</div>
<?php $this->renderView('application_views_layout',array('footer',array('validate'=>$validate))) ?>