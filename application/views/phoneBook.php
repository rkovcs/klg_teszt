<?php $this->renderView('application_views_layout',array('header',array())) ?>
<script type="text/javascript">
    $(function() {
        $(".tablesorter").tablesorter({
			sortList: [[0,0]],
            headers: {
                4: {
                    sorter: false
                }
			}
            });
    });
</script>
<table cellspacing="1" class="tablesorter">
<thead>
<tr>
    <th>First name</th>
    <th>Last name</th>
    <th>E-mail</th>
    <th>Phone</th>
    <th>Action</th>

</tr>
</thead>
<tfoot>
<tr>
    <th>First name</th>
    <th>Last name</th>
    <th>E-mail</th>
    <th>Phone</th>
    <th>Action</th>
</tr>
</tfoot>
<tbody>
<?php echo $elements ?>
</tbody>
</table>
<?php $this->renderView('application_views_layout',array('footer',array())) ?>