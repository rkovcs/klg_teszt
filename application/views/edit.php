<?php $this->renderView('application_views_layout',array('header',array())) ?>
<div class="edit">
	<fieldset>
		<legend>Edit - <?php echo (isset($first_name))?$first_name:null; echo (isset($last_name))?" ".$last_name:'new' ?></legend>
        <div id="error_box"></div>
		<form action="" method="POST" name="edit_phonebook">
			<div class="item">
				<label>First name:</label><input type="text" name="first_name" id="first_name" value="<?php echo (isset($first_name))?$first_name:null ?>">
				<label>Last name:</label><input type="text" name="last_name" id="last_name" value="<?php echo (isset($last_name))?$last_name:null ?>">
			</div>
			<div class="item"><label>E-mail:</label><input type="text" name="email" id="email" value="<?php echo (isset($email))?$email:null ?>"></div>
			<div class="item"><label>Phone<br>[vesszővel ( , ) elválasztva felsorolni]:</label>
			<textarea name="phone" id="phone" rows="5"><?php echo (isset($phone))?$phone:null ?></textarea>
			</div>
			<input type="hidden" value="<?php echo (isset($id))?$id:null ?>" name="id" id="id">
			<input type="submit" value="Save" name="submit">
			<?php if(isset($id)): ?>
			<input type="button" value="Delete" name="delete" onclick="javascript:if(confirm('Biztos, hogy törölni szeretné?'))window.location = '?c=phoneBook|delete|<?php echo $id ?>';">
			<?php endif ?>
			<input type="button" value="Cancel" name="cancel">
		</form>
	</fieldset>
</div>
<?php $this->renderView('application_views_layout',array('footer',array('validate'=>$validate))) ?>