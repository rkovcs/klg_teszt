<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class PhoneBook extends Model{


	public function __construct(){}
	/**
	 * @var string
	 */
	public $table = __CLASS__;
	/**
	 * @var string
	 */
	public $pk = 'id';

	public function findAllPhoneBookItem(){
			$sql = sprintf('SELECT
					pbook.id
					,pbook.first_name
					,pbook.last_name
					,pbook.email
					,p.phone
				FROM
					%s AS pbook
				LEFT JOIN
					%s AS p
				ON
					 pbook.id = p.id_phonebook
					',$this->table,'phone');
		$r = mysql_query($sql);
		return $r;
	}
}