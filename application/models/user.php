<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class User extends Model{


	public function __construct(){}

	/**
	 * @var string
	 */
	public $table = __CLASS__;
	/**
	 * @var string
	 */
	public $pk = 'id';
}