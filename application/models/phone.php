<?php
/**
 * Created by JetBrains PhpStorm.
 * User: roberto
 */

class Phone extends Model{

	public function __construct(){}
	/**
	 * @var string
	 */
	public $table = __CLASS__;
	/**
	 * @var string
	 */
	public $pk = 'id';

	/**
	 * @param $idPhone
	 */
	public function getPhoneBookItemNumbers($idPhone)
	{
		$query = sprintf('SELECT * FROM '.$this->table.' WHERE id_phonebook = %d',stripslashes(mysql_real_escape_string($idPhone)));
		return mysql_query($query);

	}
}